(in-package :magrathea)

(defclass magrathea-acceptor (easy-acceptor)
  ((shady-requests :initform (make-hash-table :test 'equal)
                   :accessor magrathea-shady-requests)
   (last-request-times :initform (make-hash-table :test 'equal)
                       :accessor magrathea-last-request-times)
   (sensitivity :initarg :sensitivity
                :initform 5
                :accessor magrathea-sensitivity)
   (requests-since-last-gc :initform 0)
   (warn-if-about-to-blow :initarg :warn-if-about-to-blow
                          :initform nil
                          :accessor magrathea-warn-if-about-to-blow)))


(defvar *sketchy-patterns*
  '("/wp-*")
  "A list of patterns or functions that match or return a non-NIL value if the
URL is sketchy.")

(defvar *ignore-after-time* (* 60 internal-time-units-per-second)
  "How long (in internal time units) before all previous requests are ignored.")

(defvar *warning-text* "It is most gratifying, that your enthusiasm for our server continues unabated, and so we would like to assure you that the zip bombs currently converging with your scanner are part of a special service we extend to all of our most enthusiastic clients, and the out-of-memory errors are of course merely a courtesy detail. We look forward to your custom in future lives. Thank you.

(This server uses the Magrathea software <https://gitlab.com/Theemacsshibe/magrathea> to counter scraping attempts, your browser or scraper will decompress a very, very large file if you continue to load irrelevant URLs that are used for cracking servers.)"
  "The text to respond with iff an MAGRATHEA-ACCEPTOR has WARN-IF-ABOUT-TO-BLOW set to a value that is not NIL.")

(defvar *full-acceptor-gc-requests* 1000
  "This variable sets how many requests between full garbage collections for MAGRATHEA-ACCEPTORs.")

(defun read-octets-from-file (pathname)
  (with-open-file (s pathname :element-type '(unsigned-byte 8))
    (let ((octets (make-array (file-length s))))
      (read-sequence octets s)
      octets)))

(defvar *payload-type* "gzip")
(defvar *payload* (read-octets-from-file (merge-pathnames #p"data/payload.gz" (asdf:system-source-directory :magrathea))))

(defgeneric maybe-gc-acceptor (acceptor)
  (:documentation "Possibly GC the request counts and times of an acceptor, or just increment its request counter."))

(defmethod maybe-gc-acceptor ((acceptor magrathea-acceptor))
  (with-slots (shady-requests last-request-times requests-since-last-gc) acceptor
    (let ((time (get-internal-real-time)))
      (if (< requests-since-last-gc *full-acceptor-gc-requests*)
          (incf requests-since-last-gc)
          (progn
            (setf requests-since-last-gc 0)
            (loop
               for last-request-time being the hash-values of last-request-times
               for last-request-ip   being the hash-keys   of last-request-times
               when (> (- time last-request-time) *ignore-after-time*)
               do (remhash last-request-ip last-request-times)
               do (remhash last-request-ip shady-requests)
               counting t))))))

(defmethod acceptor-dispatch-request ((acceptor magrathea-acceptor) request)
  (with-slots (shady-requests last-request-times
               sensitivity warn-if-about-to-blow requests-since-last-gc)
      acceptor
    (maybe-gc-acceptor acceptor)
    (let* ((ip (remote-addr request))
           (last-request-time (gethash ip last-request-times)))
      ;; Forgive "old" requests. See *ignore-after-time*.
      (when (and last-request-time
                 (> (- (get-internal-real-time) last-request-time)
                    *ignore-after-time*))
        (remhash ip last-request-times)
        (remhash ip shady-requests))
      ;; Match against every pattern in our *sketchy-patterns* list,
      ;; and trigger our counter-scanner code if any match.
      (let ((shady-request-count (gethash ip shady-requests 0)))
        (if (some (lambda (pattern)
                    (ignore-errors
                      (if (functionp pattern)
                          (funcall pattern (request-uri* request))
                          (scan pattern (request-uri* request)))))
                  *sketchy-patterns*)
            (progn
              (setf (gethash ip last-request-times) (get-internal-real-time))
              (cond
                ((< shady-request-count sensitivity)
                 ;; Not going to trigger yet.
                 (incf (gethash ip shady-requests 0))
                 (call-next-method))
                ((and warn-if-about-to-blow
                      (= shady-request-count sensitivity))
                 (setf (return-code*) 429
                       (content-type*) "text/plain")
                 (incf (gethash ip shady-requests))
                 *warning-text*)
                (t
                 ;; Au revoir. Oooh, bub-bub-byebye. Pick your own cliche.
                 (setf (header-out :content-encoding) *payload-type*)
                 *payload*)))
            (call-next-method))))))
